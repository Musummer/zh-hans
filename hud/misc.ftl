hud-do_not_show_on_startup = 启动时不在显示
hud-show_tips = 显示提示
hud-quests = 任务
hud-you_died = 你死了
hud-waypoint_saved = 出生点已保存
hud-sp_arrow_txt = 技能点
hud-inventory_full = 背包已满
hud-someone_else = 其他人
hud-another_group = 其他队伍
hud-owned_by_for_secs = 归属 { $name } 还有 { $secs } 秒
hud-press_key_to_show_keybindings_fmt =按 [{ $key }] 键显示按键绑定
hud-press_key_to_toggle_lantern_fmt = 按 [{ $key }] 键显示提灯
hud-press_key_to_show_debug_info_fmt = 按 [{ $key }] 键显示调试信息
hud-press_key_to_toggle_keybindings_fmt = 按 { $key } 切换显示快捷键设置
hud-press_key_to_toggle_debug_info_fmt = 按 { $key } 切换显示调试信息
hud_items_lost_dur = 你装备的物品耐久度降低了
hud-press_key_to_respawn = 按下 [{ $key }] 键在你最后访问的篝火处重生
hud-tutorial_btn = 教程
hud-tutorial_click_here = 按下 [ { $key } ] 释放鼠标光标并单击此按钮!
hud-tutorial_elements = 制作
hud-temp_quest_headline = 旅行者你好!
hud-temp_quest_text =
    想要开始旅程的话,可以探索这个村庄并收集一些物资. 

    祝你旅途愉快!

    查看屏幕的右下角,找到各种内容,例如背包,制作和地图.

    制作菜单可制作盔甲,武器,食物,道具等等!

    城镇上有很多野生动物,是皮革的重要来源,猎杀他们获得皮革,在鞣皮架制作装备,可以提升你抵御危险的能力

    只要你准备就绪,就可以尝试挑战地图上的标记点,来获得更好的装备!
hud-spell = 技能
hud-diary = 技能书
hud-free_look_indicator = 自由视角已激活.按下 [{ $key }] 键关闭.
hud-camera_clamp_indicator = 锁定视角已激活. 按下 { $key } 键关闭.
hud-auto_walk_indicator = 自动行走/游泳/滑翔已激活
hud-zoom_lock_indicator-remind = 缩放锁定
hud-zoom_lock_indicator-enable = 摄像头缩放锁定
hud-zoom_lock_indicator-disable = 摄像头缩放解锁
hud-collect = 收集
hud-pick_up = 拿起
hud-open = 打开
hud-use = 使用
hud-read = 阅读
hud-unlock-requires = 需要 { $item }
hud-unlock-consumes = 使用 { $item } 打开
hud-mine = 采集
hud-dig = 挖
hud-mine-needs_pickaxe = 需要镐子
hud-mine-needs_shovel = 需要铲子
hud-mine-needs_unhandled_case = 需要 ???
hud-talk = 对话
hud-trade = 交易
hud-mount = 攀爬
hud-sit = 坐下
hud-steer = 操纵
