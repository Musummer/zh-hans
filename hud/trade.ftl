hud-trade-trade_window = 交易
hud-trade-phase1_description =
   拖动你想交易的物品
     进入对应的区域
hud-trade-phase2_description =
    交易已锁定
     你可以对物品进行核查
hud-trade-phase3_description = 交易正在处理中.
hud-trade-persons_offer = { $playername } 的报价
hud-trade-has_accepted =
    { $playername }
    已经接受
hud-trade-accept = 接受
hud-trade-decline = 拒绝
hud-trade-invite_sent = 已向{ $playername }发送交易邀请.
hud-trade-result-completed = 交易成功.
hud-trade-result-declined = 拒绝交易.
hud-trade-result-nospace = 有足够空间来完成交易.
hud-trade-buy = Buy Price: { $coin_num ->
    [one] 一枚硬币
    *[other] { $coin_formatted } 硬币
}
hud-trade-sell = Sell Price: { $coin_num ->
    [one]  一枚硬币
    *[other] { $coin_formatted } 硬币
}
hud-trade-tooltip_hint_1 = <Shift+单击添加/移除物品.>
hud-trade-tooltip_hint_2 = <Ctrl+单击自动平衡报价.>
hud-trade-your_offer = 你的报价
hud-trade-their_offer = 对方的报价
hud-trade-amount_input = 选择一个物品
hud-confirm-trade-for-nothing = 真的要毫无代价地交出这些物品吗？
