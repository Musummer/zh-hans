## Regeneration
buff-title-heal = 治愈
buff-desc-heal = 一定时间内生命值随缓慢恢复.
buff-stat-health = 总共恢复 { $str_total } 点生命值
## Potion
buff-title-potion = 药水
buff-desc-potion = 吨吨吨...
## Saturation
buff-title-saturation = 饱腹
buff-desc-saturation = 吃饱喝足后,一定时间内生命值随缓慢恢复.
## Campfire
buff-title-campfire_heal = 篝火疗愈
buff-desc-campfire_heal = 在篝火旁休息每秒可治愈{ $rate }%生命.
## Energy Regen
buff-title-energy_regen = 耐力值恢复
buff-desc-energy_regen = 耐力值恢复提升
buff-stat-energy_regen = 恢复 { $str_total } 耐力值
## Health Increase
buff-title-increase_max_health = 增加最大生命值
buff-desc-increase_max_health = 提高你的最大生命值上限
buff-stat-increase_max_health =
    提高最大生命值
    通过 { $strength }
## Energy Increase
buff-title-increase_max_energy = 增加最大耐力值
buff-desc-increase_max_energy = 提高你的最大耐力值上限
buff-stat-increase_max_energy =
    提高最大耐力值
    通过 { $strength }
## Invulnerability
buff-title-invulnerability = 无敌
buff-desc-invulnerability = 你不会受到任何攻击的伤害.
buff-stat-invulnerability = 获得无敌状态
## Protection Ward
buff-title-protectingward = 守护领域
buff-desc-protectingward = 有股力量在守护着你,一定时间内防御得到显著提升.
## Frenzied
buff-title-frenzied = 疯狂
buff-desc-frenzied = 你激发了非同寻常的速度,能够忽略轻微的伤势.
## Haste
buff-title-hastened = 急速
buff-desc-hastened = 你的移动和攻击速度变得更快.
## Bleeding
buff-title-bleed = 流血
buff-desc-bleed = 造成一段时间的持续伤害
## Curse
buff-title-cursed = 诅咒
buff-desc-cursed = 你被诅咒了.
## Burning
buff-title-burn = 灼烧
buff-desc-burn = 你被活活烧死了
## Crippled
buff-title-crippled = 残废
buff-desc-crippled = 你的双腿受了重伤,你的运动能力受到影响.
## Freeze
buff-title-frozen = 冻结
buff-desc-frozen = 你的行动和攻击都变慢了.
## Wet
buff-title-wet = 潮湿
buff-desc-wet = 地面变得湿滑,让你很难停下来.
## Ensnared
buff-title-ensnared = 陷阱
buff-desc-ensnared = 你的腿受到束缚,阻碍了你的移动.
## Fortitude
buff-title-fortitude = 刚毅
buff-desc-fortitude = 你对于震撼的承受能力提升,并且承受越多伤害对于其他目标的震慑威力也会提升.
## Parried
buff-title-parried = 被格挡
buff-desc-parried = 你被格挡了，你的恢复更加缓慢.
## Potion sickness
buff-title-potionsickness = 药水厌倦
buff-desc-potionsickness = 因为你最近喝了一瓶药，药水给予的恢复量更少了.
buff-stat-potionsickness =
    其他药水获得的恢复量降低 { $strength }%.
## Reckless
buff-title-reckless = 鲁莽
buff-desc-reckless = 你的攻击伤害提升,但你会露出防御空隙.
## Polymorped
buff-title-polymorphed = 形态转换
buff-desc-polymorphed = 你的身体转换了形态.
## Flame
buff-title-flame = 烈焰
buff-desc-flame = 烈焰是你的盟友.
## Frigid
buff-title-frigid = 冻结
buff-desc-frigid = 冻结你的敌人.
## Lifesteal
buff-title-lifesteal = 生命吸取
buff-desc-lifesteal = 吞噬敌人的生命力.
## Salamander's Aspect
buff-title-salamanderaspect = 蜥蜴之炎
buff-desc-salamanderaspect = 你不会受到火焰伤害,并且在岩浆上移动速度更快.
## Imminent Critical
buff-title-imminentcritical = 强力一击
buff-desc-imminentcritical = 你的下一次攻击将对敌人造成暴击.
## Fury
buff-title-fury = 狂怒
buff-desc-fury = 狂怒状态下,攻击将会获得更多连击次数
## Sunderer
buff-title-sunderer = 破甲
buff-desc-sunderer = 你的攻击可以突破敌人的防御并为你恢复更多耐力.
## Sunderer
buff-title-defiance = 蔑视
buff-desc-defiance = 你能够承受更强的震撼和打击,并通过受击获得连击,但你的速度降低.
## Bloodfeast
buff-title-bloodfeast = 血宴
buff-desc-bloodfeast = 攻击流血的敌人可以恢复生命值.
## Berserk
buff-title-berserk = 狂暴
buff-desc-berserk = 进入狂暴状态,提升攻击速度,移动速度和伤害,但作为代价,防御能力降低.
## Util
buff-text-over_seconds =  { $dur_secs } 秒后结束
buff-text-for_seconds = 持续 { $dur_secs } 秒
buff-mysterious = 神秘效果
buff-remove = 点击删除
